Feature: Smoke
  As a user
  I want to test all main site functionality
  So that I can be sure that site works correctly

  Scenario Outline: Check add one product to cart for not logged in user
    Given User opens '<homePage>' page
    And User isn't logged in
    And User checks search field availability
    When User searches product by keyword '<keyword>'
    And User clicks on product title that available for shipping to user in Ukraine
    And User clicks Add to Cart
    Then User checks that product quantity on cart icon is '1'
    And User clicks on Cart icon
    And User clicks Proceed to checkout
    And User checks that Sign-In page opened
    And User checks that "Email or phone" field is visible
    And User checks that "Continue" button is visible
    And User checks that "Create your Amazon account" is visible

    Examples:
      | homePage                | keyword |
      | https://www.amazon.com/ | Laptop  |


  Scenario Outline: Check Sign in with wrong creds
    Given User opens '<homePage>' page
    And User isn't logged in
    When User clicks Sign in
    And User checks that Sign-In page opened
    And User checks that "Email or phone" field is visible
    And User checks that "Continue" button is visible
    And User enters email '<wrong_cred>'
    Then User checks that Sign-In page opened
    And User checks that Alert is displayed

    Examples:
      | homePage                | wrong_cred              |
      | https://www.amazon.com/ | Idontremembermy@mail.ua |

  Scenario Outline: Check Register
    Given User opens '<homePage>' page
    And User isn't logged in
    When User clicks Sign in
    And User checks that Sign-In page opened
    And User checks that "Create your Amazon account" is visible
    And User clicks Create your Amazon account
    Then User checks that register page opened
    And User clicks Continue
    And User checks that Name Alert is displayed
    And User checks that "Enter Email" Alert is displayed
    And User checks that Password Alert is displayed
    And User enters name '<name>'
    And User enters email '<invalid_cred>'
    And User enters password '<password>'
    And User clicks Continue
    And User checks that "Invalid email" Alert is displayed
    And User checks that "Type password again" Alert is displayed
    And User enters email '<valid_cred>'
    And User re-enters password '<password>'
    And User clicks Continue
    And User checks that puzzle page is opened

    Examples:
      | homePage                | invalid_cred         | name          | password | valid_cred              |
      | https://www.amazon.com/ | IdontremembermyEmail | NewAmazonUser | 666666   | Idontremembermy@mail.ua |

  Scenario Outline: Check Sign in
    Given User opens '<homePage>' page
    And User isn't logged in
    When User clicks Sign in
    And User checks that Sign-In page opened
    And User checks that "Email or phone" field is visible
    And User checks that "Continue" button is visible
    And User enters email '<email>'
    And User enters password '<password>' to sign in
    And User clicks submit
    Then User checks he is logged in as '<username>'

    Examples:
      | homePage                | email                    | password   | username |
      | https://www.amazon.com/ | mr_robot_2021@rambler.ua | Iwant2EPAM | Tester   |

  Scenario Outline: Check select delivery country
    Given User opens '<homePage>' page
    When User clicks Deliver to
    And User checks Choose your location popup is displayed
    And User clicks country selector
    Then User selects country '<country>' and checks it matches desired
    And User clicks done
    And User checks search field availability
    And User searches product by keyword '<keyword>'
    And User clicks on product title that available for shipping to user in '<country>'
    And User clicks Add to Cart
    And User checks that product quantity on cart icon is '<number>'

    Examples:
      | homePage                | country | keyword     | number |
      | https://www.amazon.com/ | Belarus | smart watch | 1      |

  Scenario Outline: Check catalogue menu
    Given User opens '<homePage>' page
    When User clicks on menu
    And User checks menu is displayed
    And User clicks '<category>'
    And User checks '<category>' title is displayed
    And User clicks '<menu item>'
    Then User checks search result page is opened

    Examples:
      | homePage                | category  | menu item           |
      | https://www.amazon.com/ | Computers | Computer Components |

  Scenario Outline: Check search results sorting and filtering
    Given User opens '<homePage>' page
    And User checks search field availability
    And User searches product by keyword '<keyword>'
    And User checks search result page is opened
    When User clicks sort button
    And User checks dropdown menu is displayed
    And User clicks sort by '<sort>'
    And User checks sorting by '<sort title>' is selected
    And User clicks on '<brand>' brand filter
    And User clicks on product title that available for shipping to user in Ukraine
    Then User checks product name contain '<brand>'
    And User checks that "Add to cart" button is visible

    Examples:
      | homePage                | keyword    | sort | sort title      | brand   |
      | https://www.amazon.com/ | Smartphone | 4    | Newest Arrivals | Samsung |

  Scenario Outline: Check search results switching pages
    Given User opens '<homePage>' page
    And User checks search field availability
    And User searches product by keyword '<keyword>'
    And User checks search result page is opened
    When User clicks page '<pageNO>'
    And User checks '<pageNO>' is opened
    And User clicks page '"Next"'
    And User checks next page after '<pageNO>' is opened
    And User clicks page '"Previous"'
    Then User checks '<pageNO>' is opened


    Examples:
      | homePage                | keyword    | pageNO |
      | https://www.amazon.com/ | Headphones | 3      |

  Scenario Outline: Check instant checkout with Buy Now button
    Given User opens '<homePage>' page
    And User clicks Sign in
    And User checks that Sign-In page opened
    And User checks that "Email or phone" field is visible
    And User checks that "Continue" button is visible
    And User enters email '<email>'
    And User enters password '<password>' to sign in
    And User clicks submit
    And User checks he is logged in as '<username>'
    And User checks search field availability
    And User searches product by keyword '<keyword>'
    And User clicks on product title that available for shipping to user in Ukraine
    When User clicks Buy Now
    Then User checks address select page is opened

    Examples:
      | homePage                | email                    | password   | username | keyword    |
      | https://www.amazon.com/ | mr_robot_2021@rambler.ua | Iwant2EPAM | Tester   | Desktop pc |

  Scenario Outline: Check delivery address creation
    Given User opens '<homePage>' page
    And User clicks Sign in
    And User checks that Sign-In page opened
    And User checks that "Email or phone" field is visible
    And User checks that "Continue" button is visible
    And User enters email '<email>'
    And User enters password '<password>' to sign in
    And User clicks submit
    And User checks he is logged in as '<username>'
    And User checks search field availability
    And User searches product by keyword '<keyword>'
    And User clicks on product title that available for shipping to user in Ukraine
    And User clicks Buy Now
    And User checks address select page is opened
    When User selects country '<country>'
    And User inputs Full name '<name>'
    And User inputs street address '<street>'
    And User inputs building'<apt>'
    And User inputs City '<City>'
    And User inputs State '<State>'
    And User inputs Postal Code '<ZIP>'
    And User inputs phone number '<phone>'
    And User clicks Use this address
    And User clicks Continue on shipping options page
    Then User checks Payment page is opened or Payment Method is requested


    Examples:
      | homePage                | email                    | password   | username | keyword    | country | name          | street   | apt   | City | State | ZIP   | phone      |
      | https://www.amazon.com/ | mr_robot_2021@rambler.ua | Iwant2EPAM | Tester   | Desktop pc | Ukraine | Tester Junior | Baker St | 221-b | Kyiv | Kyiv  | 04128 | 0996661613 |



