package manager;

import org.openqa.selenium.WebDriver;
import pages.*;

public class PageFactoryManager {

    WebDriver driver;

    public PageFactoryManager(WebDriver driver) {
        this.driver = driver;
    }

    public HomePage getHomePage() {
        return new HomePage(driver);
    }

    public CartPage getCartPage() {
        return new CartPage(driver);
    }

    public SearchResultsPage getSearchResultsPage() {
        return new SearchResultsPage(driver);
    }

    public ProductPage getProductPage() {
        return new ProductPage(driver);
    }

    public AddressSelectPage getAddressSelectPage() {
        return new AddressSelectPage(driver);
    }

    public SignInPage getSignInPage() {
        return new SignInPage(driver);
    }

    public RegisterPage getRegisterPage() {
        return new RegisterPage(driver);
    }
}
