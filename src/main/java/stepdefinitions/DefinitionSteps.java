package stepdefinitions;


import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import manager.PageFactoryManager;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import pages.*;

import static io.github.bonigarcia.wdm.WebDriverManager.chromedriver;
import static java.lang.Thread.sleep;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

public class DefinitionSteps {

    private static final long DEFAULT_TIMEOUT = 60;
    WebDriver driver;
    HomePage homePage;
    CartPage cartPage;
    SearchResultsPage searchResultsPage;
    ProductPage productPage;
    AddressSelectPage addressSelectPage;
    SignInPage signInPage;
    PageFactoryManager pageFactoryManager;
    RegisterPage registerPage;

    @Before
    public void testsSetUp() {
        chromedriver().setup();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        pageFactoryManager = new PageFactoryManager(driver);
    }

    @Given("User opens {string} page")
    public void openHomePage(final String url) {
        homePage = pageFactoryManager.getHomePage();
        homePage.openPage(url);
    }

    @And("User isn't logged in")
    public void userIsNotLoggedIn() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getSignInButton());
        assertTrue(homePage.getAccountGreeting().contains("Sign in"));
    }

    @And("User checks search field availability")
    public void checkSearchAvailability() {
        homePage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getSearchField());
        homePage.searchFieldIsDisplayed();
    }

    @And("User searches product by keyword {string}")
    public void enterKeywordToSearchField(final String keyword) {
        homePage.enterTextToSearchField(keyword);
    }

    @And("User clicks on product title that available for shipping to user in Ukraine")
    public void clickOnAvailableForShippingProduct() {
        searchResultsPage = pageFactoryManager.getSearchResultsPage();
        searchResultsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, searchResultsPage.getAvailableForShippingProduct());
        searchResultsPage.clickOnAvailableForShippingProduct();
    }

    @And("User clicks Add to Cart")
    public void clickAddToCart() {
        productPage = pageFactoryManager.getProductPage();
        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, productPage.getAdd2CartButton());
        productPage.click2ToCartButton();
    }

    @Then("User checks that product quantity on cart icon is {string}")
    public void checkQuantityOnCartIcon(final String expectedAmount) throws InterruptedException {
        productPage.waitForPageLoadComplete(10);
        productPage.waitForAjaxToCompleteIfAny(10);
        productPage.closeSideSheet();
        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, productPage.getCartCounter());
        assertEquals(expectedAmount, productPage.getCartCounter().getText());
    }

    @And("User clicks on Cart icon")
    public void clickOnCart() {
        productPage.clickOnCartIcon();
    }

    @And("User clicks Proceed to checkout")
    public void toCheckout() {
        cartPage = pageFactoryManager.getCartPage();
        cartPage.clickCheckout();
    }

    @And("User checks that Sign-In page opened")
    public void checkSignInURL() {
        signInPage = pageFactoryManager.getSignInPage();
        assertTrue(driver.getCurrentUrl().contains("ap/signin"));
    }

    @And("User checks that \"Email or phone\" field is visible")
    public void checkCredsField() {
        signInPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(signInPage.isInputFieldVisible());
    }

    @And("User checks that \"Continue\" button is visible")
    public void checkContinueBtn() {
        assertTrue(signInPage.isContinueBtnVisible());
    }

    @And("User checks that \"Create your Amazon account\" is visible")
    public void checkCreateAcc() {
        assertTrue(signInPage.isCreateAccBtnVisible());
    }

    @When("User clicks Sign in")
    public void clickSignIn() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getSignInButton());
        homePage.clickOnSignIn();
    }

    @And("User enters email {string}")
    public void enterEmail(String email) {
        signInPage.enterEmailToField(email);
    }

    @And("User checks that Alert is displayed")
    public void checkAlertIsDisplayed() {
        assertTrue(signInPage.alertIsDisplayed());
    }

    @And("User clicks Create your Amazon account")
    public void clickCreateAccount() {
        signInPage.clickCreateAcc();
    }

    @Then("User checks that register page opened")
    public void checkRegURL() {
        registerPage = pageFactoryManager.getRegisterPage();
        assertTrue(driver.getCurrentUrl().contains("ap/register"));
    }

    @And("User clicks Continue")
    public void clickContinue() {
        registerPage.clickContinue();
    }

    @And("User checks that Name Alert is displayed")
    public void checkNameAlert() {
        registerPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, registerPage.getNameAlert());
        assertTrue(registerPage.getNameAlert().isDisplayed());
    }

    @And("User checks that \"Enter Email\" Alert is displayed")
    public void checkEnterEmailAlert() {
        registerPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, registerPage.getEnterEmailAlert());
        assertTrue(registerPage.getEnterEmailAlert().isDisplayed());
    }

    @And("User checks that Password Alert is displayed")
    public void checkPasswordAlert() {
        registerPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, registerPage.getPasswordAlert());
        assertTrue(registerPage.getPasswordAlert().isDisplayed());
    }

    @And("User enters name {string}")
    public void enterName(String name) {
        registerPage.enterNameToNameField(name);
    }

    @And("User enters password {string}")
    public void enterPassword(String password) {
        registerPage.enterPswrd(password);
    }

    @And("User checks that \"Invalid email\" Alert is displayed")
    public void checkInvalidEmailAlert() {
        registerPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(registerPage.getInvalidEmailAlert().get(0).isDisplayed() || registerPage.getInvalidEmailAlert().get(1).isDisplayed());
    }

    @And("User checks that \"Type password again\" Alert is displayed")
    public void checkMissingPswrdAlert() {
        assertTrue(registerPage.getMissingPswrdAlert().isDisplayed());
    }

    @And("User re-enters password {string}")
    public void reEnterPassword(String password) {
        registerPage.enterToPasswordCheckField(password);
    }

    @And("User checks that puzzle page is opened")
    public void checkPuzzleURL() {
        assertTrue(driver.getCurrentUrl().contains("ap/cvf"));
    }

    @And("User enters password {string} to sign in")
    public void enterPasswordSignIn(String password) {
        signInPage.enterPswrd(password);
    }

    @And("User clicks submit")
    public void clickSubmit() {
        signInPage.clickSignInSubmit();
    }

    @Then("User checks he is logged in as {string}")
    public void userIsLoggedIn(String username) {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getSignInButton());
        assertTrue(homePage.getAccountGreeting().contains(username));
    }

    @When("User clicks Deliver to")
    public void clickDeliverTo() {
        homePage.clickOnDeliverTo();
    }

    @And("User checks Choose your location popup is displayed")
    public void checkLocationPopup() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getChooseLocPopup());
        assertTrue(homePage.getChooseLocPopup().isDisplayed());
    }

    @And("User clicks country selector")
    public void clickCountryDropdown() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getCountryListDropDown());
        homePage.clickCountryListDropDown();
    }

    @And("User selects country {string} and checks it matches desired")
    public void clickOnCountry(String country) {
        homePage.clickCountryLi();
        assertEquals(country, homePage.getCountryListDropDown().getText());
    }

    @And("User clicks done")
    public void clickDone() {
        homePage.clickDoneButton();
        homePage.waitForPageLoadComplete(10);
        homePage.waitForAjaxToCompleteIfAny(10);
    }

    @And("User clicks on product title that available for shipping to user in {string}")
    public void clickOnAvailableForShippingTo(String country) {
        searchResultsPage = pageFactoryManager.getSearchResultsPage();
        searchResultsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, searchResultsPage.getAvailableForShippingTo(country));
        searchResultsPage.clickOnAvailableForShippingTo(country);
    }

    @When("User clicks on menu")
    public void clickOnMenu() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getMenu());
        homePage.clickMenu();
    }

    @Then("User checks menu is displayed")
    public void checkMenuIsDisplayed() {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getMenu());
        assertTrue(homePage.getMenuBlock().isDisplayed());
    }

    @And("User clicks {string}")
    public void clickCategory(String category) {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getCategory(category));
        homePage.clickCategory(category);
    }

    @And("User checks {string} title is displayed")
    public void checkCategoryIsDisplayed(String category) {
        homePage.waitVisibilityOfElement(DEFAULT_TIMEOUT, homePage.getCategoryTitle(category));
        assertEquals(category, homePage.getCategoryTitle(category).getText());
    }

    @And("User checks search result page is opened")
    public void checkSearchURL() {
        assertTrue(driver.getCurrentUrl().contains("amazon.com/s"));
    }

    @When("User clicks sort button")
    public void clickSortButton() {
        searchResultsPage = pageFactoryManager.getSearchResultsPage();
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        searchResultsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, searchResultsPage.getSortButton());
        searchResultsPage.clickOnSortButton();
    }

    @And("User checks dropdown menu is displayed")
    public void checkDropDownSortMenuIsDisplayed() {
        searchResultsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, searchResultsPage.getSortDropDown());
        assertTrue(searchResultsPage.getSortDropDown().isDisplayed());
    }

    @And("User clicks sort by {string}")
    public void clickSortBy(String sort) {
        searchResultsPage.clickSortBy(sort);
    }

    @And("User clicks on {string} brand filter")
    public void clickOnBrandCheckbox(String brand) {
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        searchResultsPage.waitForAjaxToCompleteIfAny(10);
        searchResultsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, searchResultsPage.getBrandCheckBox(brand));
        searchResultsPage.getBrandCheckBox(brand).click();
    }

    @And("User checks sorting by {string} is selected")
    public void checkSortinBy(String sortTitle) {
        searchResultsPage.waitForAjaxToCompleteIfAny(10);
        searchResultsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, searchResultsPage.getSortButtonText());
        assertEquals(sortTitle, searchResultsPage.getSortButtonText().getText());
    }

    @Then("User checks product name contain {string}")
    public void productTitleContains(String brand) {
        productPage = pageFactoryManager.getProductPage();
        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, productPage.getProductTitle());
        assertTrue(productPage.getProductTitle().getText().contains(brand));
    }

    @And("User checks that \"Add to cart\" button is visible")
    public void checkAdd2CartButtonVisible() {
        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, productPage.getAdd2CartButton());
        assertTrue(productPage.getAdd2CartButton().isDisplayed());
    }

    @When("User clicks page {string}")
    public void clickPageNo(String pageNo) {
        searchResultsPage = pageFactoryManager.getSearchResultsPage();
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        searchResultsPage.waitForAjaxToCompleteIfAny(10);
        searchResultsPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, searchResultsPage.getPageButton(pageNo));
        searchResultsPage.clickOnPageNo(pageNo);
    }

    @And("User checks {string} is opened")
    public void checkPageNoIsOpened(String pageNo) {
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(driver.getCurrentUrl().contains("page=" + pageNo));
    }

    @And("User checks next page after {string} is opened")
    public void checkNextPageIsOpened(String pageNo) {
        searchResultsPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        Integer NextPageNo = Integer.parseInt(pageNo) + 1;
        assertTrue(driver.getCurrentUrl().contains("page=" + NextPageNo));
    }

    @When("User clicks Buy Now")
    public void clickBuyNow() {
        productPage = pageFactoryManager.getProductPage();
        productPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, productPage.getBuyNowButton());
        productPage.clickOnBuyNowButton();
    }

    @Then("User checks address select page is opened")
    public void checkAddressSelectPageOpened() {
        addressSelectPage = pageFactoryManager.getAddressSelectPage();
        addressSelectPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
        assertTrue(driver.getCurrentUrl().contains("/addressselect/"));
    }

    @And("User selects country {string}")
    public void selectCountry(String country) {
        addressSelectPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, addressSelectPage.getCountrySelectDropdown());
        addressSelectPage.clickCountrySelectDropdown();
        addressSelectPage.waitVisibilityOfElement(DEFAULT_TIMEOUT, addressSelectPage.countrySelector(country));
        addressSelectPage.countrySelector(country).click();
        addressSelectPage.waitForAjaxToCompleteIfAny(10);
    }

    @And("User inputs Full name {string}")
    public void inputName(String name) {
        addressSelectPage.enterTextToField(addressSelectPage.getNameField(), name);
    }

    @And("User inputs street address {string}")
    public void inputStreetAdress(String address) {
        addressSelectPage.enterTextToField(addressSelectPage.getAddressLine1(), address);
    }

    @And("User inputs building{string}")
    public void inputApt(String apt) {
        addressSelectPage.enterTextToField(addressSelectPage.getAddressLine2(), apt);
    }

    @And("User inputs City {string}")
    public void inputCity(String city) {
        addressSelectPage.enterTextToField(addressSelectPage.getAddressCity(), city);
    }

    @And("User inputs State {string}")
    public void inputState(String state) {
        addressSelectPage.enterTextToField(addressSelectPage.getAddressState(), state);
    }

    @And("User inputs Postal Code {string}")
    public void inputZIP(String ZIP) {
        addressSelectPage.enterTextToField(addressSelectPage.getAddressZIP(), ZIP);
    }

    @And("User inputs phone number {string}")
    public void inputPhone(String phone) {
        addressSelectPage.enterTextToField(addressSelectPage.getPhoneNumber(), phone);
    }

    @And("User clicks Use this address")
    public void clickUseThisAddress() {
        addressSelectPage.clickAddressSubmit();
    }

    @And("User clicks Continue on shipping options page")
    public void clickContinueOptions() {
        try {
            addressSelectPage.waitForPageLoadComplete(DEFAULT_TIMEOUT);
            addressSelectPage.waitVisibilityOfElement(10, addressSelectPage.getContinueBtn());
            addressSelectPage.clickContinue();
        } catch (TimeoutException e) {
        }
        ;
    }

    @And ("User checks Payment page is opened or Payment Method is requested")
    public void checkPaymentPageIsOpened(){
        assertTrue(driver.getCurrentUrl().contains("/payselect/")||
                addressSelectPage.getAddCreditDebitCardBtn().isDisplayed());
    }

    @After
    public void tearDown() {
        driver.close();
    }

}
