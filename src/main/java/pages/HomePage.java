package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import java.util.List;
import java.util.Locale;

public class HomePage extends BasePage {

    @FindBy(xpath = "//*[@id=\"nav-link-accountList-nav-line-1\"]")
    private WebElement account;

    @FindBy(xpath = "//*[@id=\"twotabsearchtextbox\"]")
    private WebElement searchField;

    @FindBy(xpath = "//*[@id=\"nav-link-accountList\"]/span")
    private WebElement signIn;

    @FindBy(xpath = "//*[@id=\"nav-global-location-popover-link\"]")
    private WebElement deliverTo;

    @FindBy(xpath = "//*[contains(@id,\"a-popover-content\")]")
    private WebElement chooseLocPopup;

    @FindBy(xpath = "//*[@id=\"GLUXCountryValue\"]")
    private WebElement countryListDropDown;

    @FindBy(xpath = "//a[contains(@id,\"GLUXCountryList_26\")][contains(text(),\"Belarus\")]")
    private List<WebElement> countryLI;

    @FindBy(xpath = "//*[@name=\"glowDoneButton\"]")
    private WebElement doneButton;

    @FindBy(xpath = "//*[@id=\"nav-hamburger-menu\"]")
    private WebElement menu;

    @FindBy(xpath = "//*[@id=\"hmenu-canvas\"]")
    private WebElement menuBlock;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public void openPage(String url) {
        driver.get(url);
    }

    public String getAccountGreeting() {
        return account.getText();
    }

    public void searchFieldIsDisplayed() {
        searchField.isDisplayed();
    }

    public void enterTextToSearchField(final String searchText) {
        searchField.clear();
        searchField.sendKeys(searchText, Keys.ENTER);
    }

    public WebElement getSearchField() {
        return searchField;
    }

    public WebElement getSignInButton() {
        return signIn;
    }

    public void clickOnSignIn() {
        signIn.click();
    }

    public void clickOnDeliverTo() {
        deliverTo.click();
    }

    public WebElement getChooseLocPopup() {
        return chooseLocPopup;
    }

    public void clickCountryListDropDown() {
        countryListDropDown.click();
    }

    public WebElement getCountryListDropDown() {
        return countryListDropDown;
    }

    public void clickCountryLi() {
        new Actions(driver).moveToElement(countryLI.get(0)).click().perform();
    }

    public void clickDoneButton() {
        doneButton.click();
    }

    public WebElement getMenu() {
        return menu;
    }

    public void clickMenu() {
        menu.click();
    }

    public WebElement getMenuBlock() {
        return menuBlock;
    }

    public WebElement getCategory(String category) {
        return driver.findElement(By.xpath("//*[@id=\"hmenu-content\"]//*[contains(text(),\"" + category + "\")]"));
    }

    public void clickCategory(String category) {
        getCategory(category).click();
    }

    public WebElement getCategoryTitle(String category) {
        return driver.findElement(By.xpath("//*[@id=\"hmenu-content\"]//div[text()=\"" + category.toLowerCase() + "\"]"));
    }
}



