package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class RegisterPage extends BasePage {

    @FindBy(xpath = "//*[@id=\"continue\"]")
    private WebElement continueBtn;

    @FindBy(xpath = "//div[contains(text(),\"Enter your name\")]")
    private WebElement nameAlert;

    @FindBy(xpath = "//div[contains(text(),\"Enter your email\")]")
    private WebElement enterEmailAlert;

    @FindBy(xpath = "//*[@id=\"auth-password-missing-alert\"]/div/div")
    private WebElement passwordAlert;

    @FindBy(xpath = "//*[@id=\"ap_customer_name\"]")
    private WebElement nameField;

    @FindBy(xpath = "//*[@id=\"ap_password\"]")
    private WebElement passwordField;

    @FindBy(xpath = "//*[contains(@id,\"auth-email-invalid\")]/div/div")
    private List<WebElement> invalidEmailAlert;

    @FindBy(xpath = "//*[@id=\"auth-passwordCheck-missing-alert\"]/div/div")
    private WebElement missingPswrdAlert;

    @FindBy(xpath = "//*[@id=\"ap_password_check\"]")
    private WebElement passwordCheckField;

    public RegisterPage(WebDriver driver) {
        super(driver);
    }

    public void clickContinue() {
        continueBtn.click();
    }

    public WebElement getNameAlert(){
        return nameAlert;
    }

    public WebElement getEnterEmailAlert(){
        return enterEmailAlert;
    }

    public WebElement getPasswordAlert(){
        return passwordAlert;
    }

    public void enterNameToNameField(String name){
        nameField.clear();
        nameField.sendKeys(name);
    }
    
    public void enterPswrd(String password){
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    public List<WebElement> getInvalidEmailAlert() {
        return invalidEmailAlert;
    }

    public WebElement getMissingPswrdAlert() {
        return missingPswrdAlert;
    }

    public void enterToPasswordCheckField(String password){
        passwordCheckField.clear();
        passwordCheckField.sendKeys(password);
    }
}
