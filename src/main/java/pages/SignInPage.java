package pages;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class SignInPage extends BasePage {

    @FindBy(xpath = "//*[@id=\"ap_email\"]")
    private WebElement inputField;

    @FindBy(xpath = "//*[@id=\"continue\"]")
    private WebElement continueBtn;

    @FindBy(xpath = "//*[@id=\"createAccountSubmit\"]")
    private WebElement createAccBtn;

    @FindBy(xpath = "//*[@id=\"auth-error-message-box\"]")
    private WebElement alert;

    @FindBy(xpath = "//*[@id=\"ap_password\"]")
    private WebElement passwordField;

    @FindBy(xpath = "//*[@id=\"signInSubmit\"]")
    private WebElement signInSubmit;

    public SignInPage(WebDriver driver) {
        super(driver);//*[@id="ap_email"]
    }

    public boolean isInputFieldVisible() {
        return inputField.isDisplayed();
    }

    public boolean isContinueBtnVisible(){
        return continueBtn.isDisplayed();
    }

    public boolean isCreateAccBtnVisible() {
        return createAccBtn.isDisplayed();
    }

    public void clickCreateAcc(){
        createAccBtn.click();
    }

    public void enterEmailToField(final String email) {
        inputField.clear();
        inputField.sendKeys(email, Keys.ENTER);
    }

    public void enterPswrd(String password){
        passwordField.clear();
        passwordField.sendKeys(password);
    }

    public boolean alertIsDisplayed(){
        return alert.isDisplayed();
    }

    public void clickSignInSubmit(){
        signInSubmit.click();
    }
}
