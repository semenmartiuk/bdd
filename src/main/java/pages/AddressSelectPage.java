package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import javax.security.auth.x500.X500Principal;

public class AddressSelectPage extends BasePage {

    public AddressSelectPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@id=\"address-ui-widgets-countryCode\"]")
    private WebElement countrySelectDropdown;

    @FindBy(xpath = "//*[@id=\"address-ui-widgets-enterAddressFullName\"]")
    private WebElement nameField;

    @FindBy(xpath = "//*[@id=\"address-ui-widgets-enterAddressLine1\"]")
    private WebElement addressLine1;

    @FindBy(xpath = "//*[@id=\"address-ui-widgets-enterAddressLine2\"]")
    private WebElement addressLine2;

    @FindBy(xpath = "//*[@id=\"address-ui-widgets-enterAddressCity\"]")
    private WebElement addressCity;

    @FindBy(xpath = "//*[@id=\"address-ui-widgets-enterAddressStateOrRegion\"]")
    private WebElement addressState;

    @FindBy(xpath = "//*[@id=\"address-ui-widgets-enterAddressPostalCode\"]")
    private WebElement addressZIP;

    @FindBy(xpath = "//*[@id=\"address-ui-widgets-enterAddressPhoneNumber\"]")
    private WebElement phoneNumber;

    @FindBy(xpath = "//*[@id=\"address-ui-widgets-form-submit-button\"]/span/input")
    private WebElement addressSubmit;

    @FindBy(xpath = "//*[@id=\"shippingOptionFormId\"]//input[@Value=\"Continue\"][@data-testid]")
    private WebElement continueBtn;

    @FindBy(xpath = "//span[contains(text(),\"Add a credit or debit card\")]")
    private WebElement addCreditDebitCardBtn;

    public WebElement getCountrySelectDropdown(){
        return countrySelectDropdown;
    }

    public void clickCountrySelectDropdown(){
        countrySelectDropdown.click();
    }

    public WebElement countrySelector(String country){
        return driver.findElement(By.xpath("//*[contains(@id,\"dropdown_combobox\")]/li/a[(text()=\""+country+"\")]"));
    }

    public WebElement getNameField(){
        return nameField;
    }

    public WebElement getAddressLine1(){
        return addressLine1;
    }

    public WebElement getAddressLine2(){
        return addressLine1;
    }

    public WebElement getAddressCity(){
        return addressCity;
    }

    public WebElement getAddressState(){
        return addressState;
    }

    public WebElement getAddressZIP(){
        return addressZIP;
    }

    public WebElement getPhoneNumber(){
        return phoneNumber;
    }

    public void enterTextToField(WebElement field, String text){
        field.clear();
        field.sendKeys(text);
    }

    public void clickAddressSubmit(){
        addressSubmit.click();
    }

    public WebElement getContinueBtn() {
        return continueBtn;
    }

    public void clickContinue(){
        continueBtn.click();
    }

    public WebElement getAddCreditDebitCardBtn() {
        return addCreditDebitCardBtn;
    }
}
