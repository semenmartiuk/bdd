package pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import java.util.List;

public class SearchResultsPage extends BasePage {
    @FindBy(xpath = "//span[text()=\"Ships to Ukraine\"]/../../../../../..//h2//a")
    private List<WebElement> availableForShippingProducts;

    @FindBy(xpath = "//*[contains(@id,\"announce\")]")
    private WebElement sortButton;

    @FindBy(xpath = "//*[contains(@id,\"a-popover\")][contains(@class,\"dropdown\")]")
    private WebElement sortDropDown;

    @FindBy(xpath = "//*[contains(@id,\"announce\")]/span[contains(@class,\"prompt\")]")
    private WebElement sortButtonText;

    public SearchResultsPage(WebDriver driver) {
        super(driver);
    }

    public WebElement getAvailableForShippingProduct() {
        return availableForShippingProducts.get(0);
    }

    public void clickOnAvailableForShippingProduct() {
        getAvailableForShippingProduct().click();
    }

    public WebElement getAvailableForShippingTo(String country) {
        return driver.findElements(By.xpath("//span[text()=\"Ships to " + country + "\"]/../../../../../..//h2//a")).get(0);
    }

    public void clickOnAvailableForShippingTo(String country) {
        getAvailableForShippingTo(country).click();
    }

    public WebElement getSortButton() {
        return sortButton;
    }

    public void clickOnSortButton() {
        sortButton.click();
    }

    public WebElement getSortDropDown() {
        return sortDropDown;
    }

    public void clickSortBy(String sort) {
        driver.findElement(By.xpath("//*[@id=\"s-result-sort-select_" + sort + "\"]")).click();
    }

    public WebElement getSortButtonText() {
        return sortButtonText;
    }

    public WebElement getBrandCheckBox(String brand) {
        return driver.findElement(By.xpath("//*[contains(@id,\"" + brand + "\")]//a"));
    }

    public WebElement getPageButton(String pageNo) {
        return driver.findElement(By.xpath("//ul[@class=\"a-pagination\"]/li/a[text()=" + pageNo + "]"));
    }

    public void clickOnPageNo(String pageNo) {
        getPageButton(pageNo).click();
    }

}
