package pages;

import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class ProductPage extends BasePage {

    public ProductPage(WebDriver driver) {
        super(driver);
    }

    @FindBy(xpath = "//*[@id=\"add-to-cart-button\"]")
    private WebElement add2CartButton;

    @FindBy(xpath = "//*[@id=\"nav-cart-count\"]")
    private WebElement cartCounter;

    @FindBy(xpath = "//*[@id=\"nav-cart-text-container\"]//span[@class=\"nav-line-2\"]")
    private WebElement cartIcon;

    @FindBy(xpath = "//*[@id=\"attach-close_sideSheet-link\"]")
    private WebElement closeSideSheetLink;

    @FindBy(xpath = "//*[@id=\"productTitle\"]")
    private WebElement productTitle;

    @FindBy(xpath = "//*[@id=\"buy-now-button\"]")
    private WebElement buyNowButton;

    public WebElement getAdd2CartButton() {
        return add2CartButton;
    }

    public void click2ToCartButton() {
        add2CartButton.click();
    }

    public WebElement getCartCounter() {
        return cartCounter;
    }

    public void clickOnCartIcon() {
        cartIcon.click();
    }

    public void closeSideSheet() {
        try {
            waitVisibilityOfElement(10, closeSideSheetLink);
            closeSideSheetLink.click();
        } catch (TimeoutException e) {

        }
    }

    public WebElement getProductTitle() {
        return productTitle;
    }

    public WebElement getBuyNowButton(){
        return buyNowButton;
    }

    public void clickOnBuyNowButton(){
        buyNowButton.click();
    }
}
